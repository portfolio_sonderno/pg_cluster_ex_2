#!/bin/bash

if [ $# -ne 1 ]; then
    echo "Usage: $0 <ip_address>"
    exit 1
fi

ip_address=$1
default_interface=$(ip route | awk '/default/ {print $5}')
default_ip=$(ip addr show dev $default_interface | awk '/inet / {print $2}')

if ! [[ $ip_address =~  $default_ip ]]; then
    echo "IP address not set on default interface. Setting IP address..."
    ip addr add $ip_address/16 dev $default_interface
    echo "IP address set to $ip_address on interface $default_interface"
    exit 0
else
    echo "IP address $default_ip already set on interface $default_interface"
    exit 1
fi