package master

import (
	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
	"github.com/rs/zerolog/log"
	"os/exec"
	"pg_cluster_agent/internal/cluster"
	"pg_cluster_agent/internal/config"
)

//func RunMaster(ct *cluster.Cluster, cfg *config.Config) {
//	log.Info().Msg("Run as Master")
//	cmd := exec.Command("./add_ip.sh", cfg.CLUSTER_HOST)
//	err := cmd.Run()
//	if err != nil {
//		log.Error().Msgf("We cant create cluster IP address. Error is %s", err)
//	}
//	for {
//		time.Sleep(10 * time.Second)
//		arbiter, err := ct.PingArbiter()
//		if err != nil {
//			log.Error().Msgf("We cant do HTTP GET to Arbiter Host (%s). Error is %s",
//				ct.ArbiterHost, err)
//			continue
//		}
//		selfCheck := ct.MasterHost.Ping()
//		if selfCheck != nil {
//			log.Debug().Msg("Self check is not OK")
//		}
//		if arbiter == false && ct.PingSlave() == false {
//			cmd = exec.Command("./del_ip.sh", cfg.CLUSTER_HOST)
//			err = cmd.Run()
//			if err != nil {
//				log.Error().Msgf("We cant delete cluster IP address. Error is %s", err)
//			}
//			log.Info().Msg("Start blocking input connection to Master")
//
//			cmd := exec.Command("iptables", "-P", "INPUT", "DROP")
//			err := cmd.Run()
//
//			if err == nil {
//				log.Info().Msg("Success block connections to Master")
//			}
//
//			cmd = exec.Command("iptables-save", ">", "/etc/iptables/rules.v4")
//			err = cmd.Run()
//
//			if err == nil {
//				log.Info().Msg("Success save iptables changes")
//				break
//			}
//
//			log.Info().Err(err).Msg("Block input connection to Master")
//		}
//	}
//}

func RunMaster(ct *cluster.Cluster, cfg *config.Config) {
	log.Info().Msg("Run as Master")
	cmd := exec.Command("./add_ip.sh", cfg.CLUSTER_HOST)
	err := cmd.Run()
	if err != nil {
		log.Error().Msgf("We cant create cluster IP address. Error is %s", err)
	}
	server := gin.Default()
	server.GET("/shutdown", Shutdown)
	server.GET("/accept", Accept)
}

func Accept(_ *gin.Context) {
	cmd := exec.Command("iptables", "-F")
	err := cmd.Run()
	if err == nil {
		log.Info().Msg("Success block connections to Master")
	}
}

func Shutdown(_ *gin.Context) {
	cfg, err := config.Load()
	if err != nil {
		log.Err(err)
	}
	err = exec.Command("./del_ip.sh", cfg.CLUSTER_HOST).Run()
	if err != nil {
		log.Err(err).Msg("Cant delete cluster ip")
	}
	err = exec.Command("iptables", "-A", "INPUT", "-p", "tcp", "--dport", "5432", "-j", "DROP").Run()
	if err != nil {
		log.Err(err).Msg("Cannot block input d5432 connections to Master")
	}
	err = exec.Command("iptables", "-A", "INPUT", "-p", "tcp", "--sport", "5432", "-j", "DROP").Run()
	if err != nil {
		log.Err(err).Msg("Cannot block input s5432 connections to Master")
	}
}
